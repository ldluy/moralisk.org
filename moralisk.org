#+TITLE: Moralisk ekonomi: preliminära resultat
#+OPTIONS: num:nil html-postamble:nil
#+SETUPFILE: theme-readtheorg-local.setup

* Introduktion

Här presenteras de preliminära resultat från enkäten /Moralisk ekonomi/. Vi har också skrivit en rapport som du kan [[http://umu.diva-portal.org/smash/get/diva2:1503811/FULLTEXT01.pdf][läsa genom att klicka här]].

Hälsningar

Tage Alalehto, docent sociologi, Umeå universitet

Daniel Larsson, docent sociologi, Umeå universitet

* Bakgrund
Här kommer vi att skriva mer om bakgrunden till enkäten.

* Preliminära resultat

** Andel som anser att prissamarbete är olagligt
[[./p1.png]]

** Skillnad mellan män och kvinnor (andel som anser att prissamarbete är lagligt)
[[./p2.png]]

** Skillnad baserat på utbildning (andel som anser att prissamarbete är lagligt)
[[./p3.png]]

** Medelålder för de som anser att prissamarbete är lagligt/ej lagligt
[[./p4.png]]

** Betydelsen av att ha begått brott (de senaste fem åren) för attityd till prissamarbete
[[./p5.png]]

** Vilket straff bör prissamverkande företagare ha?
[[./p6.png]]


